//Punchin'

// This puff is a base for all of the next puffs. It does not have an assigned decal.
class PawnchPuff : DSPuff
{
	Default
	{
		DamageType "Fists";
		ProjectileKickback 125;
		PawnchPuff.ZKickback 0;
		+DSPUFF.FLIPDIRECTION
		+NOGRAVITY
		+NOEXTREMEDEATH
		//+BLOODLESSIMPACT //If I don't get that visual input that shit's being beaten hard, it's not funny.
		+PUFFONACTORS
		+PUFFGETSOWNER
		+ALLOWTHRUFLAGS
		+MTHRUSPECIES
	}
	
	int ZKickback;
	property ZKickback: ZKickback;
	
	// Making punching more viable.
	override void BeginPlay()
	{
		Super.BeginPlay();
		if(random(0, 100) < 40)
		{
			bFORCEPAIN = true;
		}
	}
	
	States
	{
		Spawn:
		Melee:
			TNT1 A 15 {
				A_AlertMonsters(128);
				A_StartSound("Punch/Hit", 5, volume:0.65);
				A_QuakeEx(1,1,1,4,0,96,"none",QF_SCALEDOWN);
				//A_SpawnItemEx("PunchPuff_Shock",0,0,0,0,0,0,0,SXF_CLIENTSIDE,0);
			}
			Stop;
		Crash:
			TNT1 A 15 {
				A_AlertMonsters(128);
				A_StartSound("Punch/Hit", 5, volume:0.65);
				A_StartSound("Punch/Wall", 6, volume:1.0);
				A_QuakeEx(1,1,1,4,0,96,"none",QF_SCALEDOWN);
			}
			Stop;
		XDeath:
			TNT1 A 3 {
				A_AlertMonsters(128);
				A_StartSound("Punch/Flesh", 5, volume:1.0);
				A_QuakeEx(1,1,1,4,0,96,"none",QF_SCALEDOWN);
			}
			TNT1 A 12 A_StartSound("Punch/Flavor", 6, volume:1.0);
			Stop;
	}
}

class PawnchPuffLeft : PawnchPuff //Standard Punch
{
	Default
	{
		DSPuff.Decal "PunchStampLeft";
	}
}

class PawnchPuffAltLeft : PawnchPuff //Alternate
{
	Default
	{
		DSPuff.Decal "PunchStampLeft2";
	}
}

class PawnchPuffRight : PawnchPuff
{
	Default
	{
		DSPuff.Decal "PunchStampRight";
	}
}

class PawnchPuffAltRight : PawnchPuff //Alternate
{
	Default
	{
		DSPuff.Decal "PunchStampRight2";
	}
}

class ScratchPuffLeft : PawnchPuff
{
	Default
	{
		DSPuff.Decal "ClawStampLeft";
	}

	States
	{
		Spawn:
		Melee:
			TNT1 A 0 {
				A_AlertMonsters(64);
				A_StartSound("Claw/Wall", 5, volume:0.5);
			}
			Stop;
		Crash:
			TNT1 A 0 {
				A_AlertMonsters(64);
				A_StartSound("Claw/Wall", 5, volume:0.5);
			}
			Stop;
		XDeath:
			TNT1 A 0 A_StartSound("Claw/Hit", 5, volume:0.8);
			Stop;
   }
}

class ScratchPuffAltLeft : ScratchPuffLeft
{
	Default
	{
		DSPuff.Decal "ClawStampAltLeft";
	}
}

class ScratchPuffRight : ScratchPuffLeft
{
	Default
	{
		DSPuff.Decal "ClawStampRight";
	}
}

class ScratchPuffAltRight : ScratchPuffRight
{
	Default
	{
		DSPuff.Decal "ClawStampAltRight";
	}
}

class SuperPawnchPuff : PawnchPuff
{
	Default
	{
		DSPuff.Decal "SuperPunchStamp";
		-NOEXTREMEDEATH
		+EXTREMEDEATH
		+FORCERADIUSDMG
		//+FOILINVUL
	}

	States
	{
	Spawn:
	Crash:
	XDeath:
		TNT1 A 1 {
			A_AlertMonsters(256);
			A_StartSound("SuperPunch/Hit", 0, volume:0.7);
			A_StartSound("Punch/Flavor", 6, volume:1.0);
			A_SpawnItemEx("SuperPunchPuff_Shock",0,0,0,0,0,0,0,SXF_CLIENTSIDE,0);
			A_SpawnItemEx("SuperPunchPuff_Impact",0,0,0,0,0,0,0,SXF_CLIENTSIDE,0);
			A_SpawnItemEx("SuperPunchPuff_Wave",0,0,0,0,0,0,0,SXF_CLIENTSIDE,0);
			A_QuakeEx(3,3,3,10,0,384,"none",QF_SCALEDOWN);
		}
		TNT1 A 14 A_Explode((20),48,0); //This weird setup is so the puff's damage gets applied before the explosion, so the lifedrain effect kicks in beforehand.
		Stop;
   }
}

class SuperPawnchPuffAlt : SuperPawnchPuff //Alternate
{
	Default
	{
		DSPuff.Decal "SuperPunchStamp2";
	}
}

class SuperPawnchPuffLeft : SuperPawnchPuff //Standard punch
{
	Default
	{
		DSPuff.Decal "SuperPunchStampLeft";
	}
}

class SuperPawnchPuffAltLeft : SuperPawnchPuff //Alternate
{
	Default
	{
		DSPuff.Decal "SuperPunchStampLeft2";
	}
}

class SuperPawnchPuffRight : SuperPawnchPuff //Standard punch
{
	Default
	{
		DSPuff.Decal "SuperPunchStampRight";
	}
}

class SuperPawnchPuffAltRight : SuperPawnchPuff //Alternate
{
	Default
	{
		DSPuff.Decal "SuperPunchStampRight2";
	}
}

class SuperPunchPuff_Shock : Actor
{
	Default
	{
		RenderStyle "Add";
		Scale 0.015;
		+NOINTERACTION
		+FORCEXYBILLBOARD
	}

	States
	{
	Spawn:
		SHCK D 1 Bright {
			A_SetScale(Scale.X + FRandom(0.01, 0.02));
			A_FadeOut(0.1);
		}
		Loop;
	}
}

class SuperPunchPuff_Impact : SuperPunchPuff_Shock
{
	Default
	{
		Scale 0.025;
	}

	States
	{
	Spawn:
		BLST C 0 NoDelay A_Jump(128,"AltSprite");
		Goto Looplet;
	AltSprite:
		BLST D 0;
	Looplet:
		"####" "#" 1 Bright {
			A_SetScale(Scale.X + FRandom(0.01, 0.015));
			A_FadeOut(0.125);
		}
		Loop;
	}
}

class SuperPunchPuff_Wave : SuperPunchPuff_Shock
{
	Default
	{
		Scale 0.05;
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay A_Jump(256,"SpriteA","SpriteB","SpriteC","SpriteD");
		SpriteA:
			BRWV A 0;
			Goto Looplet;
		SpriteB:
			BRWV B 0;
			Goto Looplet;
		SpriteC:
			BRWV C 0;
			Goto Looplet;
		SpriteD:
			BRWV D 0;
			Goto Looplet;
		Looplet:
			"####" "#" 1 Bright {
				A_SetScale(Scale.X + FRandom(0.01, 0.015));
				A_FadeOut(0.1);
			}
			Loop;
		}
}

class SuperScratchPuffLeft : ScratchPuffLeft
{
	Default
	{
		DamageType "SuperCloseCombat";
		Decal "SuperClawStampLeft";
		-NOEXTREMEDEATH
		+EXTREMEDEATH
		+PUFFONACTORS
		+FOILINVUL
	}

	States
	{
	SpawnMain:
	CrashMain:
		TNT1 A 15 {
			A_AlertMonsters(256);
			A_StartSound("Claw/Wall", 5, volume:0.6);
		}
		Stop;
	XDeathMain:
		TNT1 A 15 {
			A_AlertMonsters(256);
			A_StartSound("Claw/Hit", 5, volume:0.6);
		}
		Stop;
	}
}

class SuperScratchPuffAltLeft : SuperScratchPuffLeft
{
	Default
	{
		DSPuff.Decal "SuperClawStampAltLeft";
	}
}

class SuperScratchPuffRight : SuperScratchPuffLeft
{
	Default
	{
		DSPuff.Decal "SuperClawStampRight";
	}
}

class SuperScratchPuffAltRight : SuperScratchPuffLeft
{
	Default
	{
		DSPuff.Decal "SuperClawStampAltRight";
	}
}

//Uppercut
class UppercutPuff : PawnchPuff
{
	Default
	{
		DSPuff.Decal "UppercutStamp";
		ProjectileKickback 400;
		PawnchPuff.ZKickback 200;
		-NOEXTREMEDEATH
	}
}

class SuperUppercutPuff : SuperPawnchPuff
{
	Default
	{
		DSPuff.Decal "SuperUppercutStamp";
		ProjectileKickback 800;
		PawnchPuff.ZKickback 400;
		-NOEXTREMEDEATH
		+EXTREMEDEATH
		+FORCERADIUSDMG
		//+FOILINVUL
	}
}

// This series of classes added because DSProjectiles use associated puffs to create decals.
class KickPuff : DSPuff
{
	Default
	{
		DSPuff.Decal "BootStamp";
		DamageType "Melee";
		//+BLOODLESSIMPACT
		+PUFFGETSOWNER
		+ALLOWTHRUFLAGS
		+MTHRUSPECIES
	}
	
	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_StartSound("Kick/Hit");
			Stop;
	}
}

class KickPuffAlt : KickPuff
{
	Default
	{
		DSPuff.Decal "BootStampTwo";
	}
}

class KickPuffLeft : KickPuff
{
	Default
	{
		DSPuff.Decal "LeftBootStamp";
	}
}

class KickPuffRight : KickPuff
{
	Default
	{
		DSPuff.Decal "RightBootStamp";
	}
}

class SuperKickPuff : KickPuff
{
	Default
	{
		DSPuff.Decal "SuperBootStamp";
	}
	
	States
	{
		Spawn:
			TNT1 A 1 NoDelay A_StartSound("SuperKick/Hit");
			Stop;
	}
}

class SuperKickPuffAlt : SuperKickPuff
{
	Default
	{
		DSPuff.Decal "SuperBootStampTwo";
	}
}

class SuperKickPuffLeft : SuperKickPuff
{
	Default
	{
		DSPuff.Decal "SuperLeftBootStamp";
	}
}

class SuperKickPuffRight : SuperKickPuff
{
	Default
	{
		DSPuff.Decal "SuperRightBootStamp";
	}
}

