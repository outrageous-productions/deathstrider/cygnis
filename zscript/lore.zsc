class Zhs2_DSA_CygnisLoreMain : DSLoreEntry
{
	override string GetTitle() { return "Cygnis"; }
	override string GetKey() { return "CygnisSolomon"; }
	override string GetText()
	{ return
		"The first time I encountered Cygnis Flaynithere was brief, but intense. I was wandering the realms of Parthoris, in the belt of space and time I have come to understand were under the command of the void sorceror known as D'Sparil. In one of the vast caverns of a mountain range did he ambush me, a 125kg monstrosity in full red scales with a black pattern to them swinging fists the density of titanium which I deflected with Harbinger, a ridiculously huge revolver rivalling Acecorp's own Majestic, and a massive railgun capable of penetrating the landscape at will. So too did he display an uncommon if not savant-level propensity for magic, albeit only as a tool for further murder rather than cooperation with the forces that be. I dared not ask Magic if creatures like him were a headache.

		Our fight only ended when his careless shooting of an explosive slug launcher collapsed the ceiling, blocking the passage with rockfall and rendering us at impasse for further violence. Only then did I hear him speak.

		\c[TCol_Cygnis]''You won't be claiming my life, reaper! Nor shall you have my targets either! I will be responsible for the death of the Serpent Rider! Me!''\c-
		\c[TCol_Solomon]''Apparently your time has yet to come. You may, however, have trouble finishing your quest if your tactics are as good as your aim.''\c-
		\c[TCol_Cygnis]''I won't hear that from a despicable wazard \c-(sic?)\c[TCol_Cygnis] like you! I WILL have my vengeance on D'Sparil - curse his name - and nothing shall stand in my way!''\c-

		That was all I heard from him on that day, his harsh footsteps following with his angry muttering and echoing quieter and quieter through the cavern. I needed to secure an alternate route from then on to complete my own expedition and I filed him as another potential foe in my mind. I was not expecting the moment I met him again somewhere around twenty years later, at the exit to a similar yet far removed cave. Instinctively I readied my weapons, wary of the sidearm in his holster and that intimidating railgun at his back, but he made no move to attack - only glare with insistence in his serpent-like eyes.

		\c[TCol_Cygnis]''You still yet stalk these accursed halls of stone, reaper? I was sure I flushed out every spellslinger and underling within a hundred kilometers.''\c-
		\c[TCol_Solomon]''Permit me to ask why you choose to stare into death instead of assaulting it this time.'\c-
		\c[TCol_Cygnis]''My quest, as you so called it then, is complete. I've no immediate need to be rash. I still have a life to live, a wife and children to feed, and a tenant under my roof of... complicated standing. If you have come for them or I, then my mood will dip verily. But I have seen you operate, and I believe I can make you an offer. My service for your gold. Whatever you so point me at shall be obliterated.''\c-
		\c[TCol_Solomon]\''I do not have gold, per se, but I can arrange your kin to be well fed provided you do not expire on the job.''\c-
		\c[TCol_Cygnis]''Your slights go unappreciated. I needn't prove myself to some eternal avatar, not when I've faced enough death to make you or whomever takes souls to the afterlife blush.''\c-
		\c[TCol_Solomon]''If you are to be offering your services I would prefer you do prove yourself. Accompany me for the remainder of this campaign and I will consider the experience to be your résumé.''\c-
		\c[TCol_Cygnis]''Tch. I work best alone, sack of bones. And I would much prefer gold coins like of my homeland over mere pledges of food. But since there are no other strange mercenary fellows around capable of offering me space travel over wandering interdimensional rifts for no pay, I suppose I don't have much choice.''\c-
		\c[TCol_Solomon]''Perform well enough, and I will see what I can do about that gold.''\c-";
	}
}

class Zhs2_DSA_CygnisLoreGuest : DSLoreEntry
{
	override string GetTitle() { return "Big Dumb Lizard"; }
	override string GetKey() { return "CygnisVisse"; }
	override int GetTerminalIndex() { return TIndex_GuestRoom; }
	override string GetText()
	{ return
		"Title as provided by Wraith. Solomon keeps a varied crew onboard this ship but none so far have been so storybook as a bipedal dragon, in my opinion. I would know, I too have horns and wings. I have seen Solomon giving said dragon - whom I have come to learn is named Cygnis - a watchful, almost piercing eye when he knows Cygnis is not looking, but as far as I have been able to glean he was the one who brought the fiery creature aboard. Wraith and Cygnis do not quite get along in conversation, but whenever they go on a mission together they work almost like two halves of the same person. The mercenary mindset at work, I am guessing, and it makes the title of this log feel more endearing, in my opinion. More of a feat is that Cygnis is not afraid to tell Solomon what he thinks or the Traveler ''up yours''. I'm not quite sure whether he is very brave or very stupid, but there is an aura to him even I cannot deny.
		
		... On the subject, I have noticed that his gaze lingers on me at times, and is quick to avert whenever I notice. He is all business aside from this one shortcoming, and while I am appropriately intimidated by him, the urge to ask why becomes stronger every time this happens. Should I dare? Would I dare?
		
		ADDENDUM: I dared. I figured it was only a matter of time until I am the only supplier able to head out on a mission with Cygnis so it would be best to get this awkwardness out of the way. It was brief enough to commit to memory:
		
		\c[TCol_Visse]''Excuse me. Is there something about me that bothers you?''\c-
		
		A fair pause, and a gaunt stare. If I were anyone else I might have shied away from his brimstone breath.
		
		\c[TCol_Cygnis]''No.''\c-
		\c[TCol_Visse]''Then why do your eye movements gain friction in my direction?''\c-
		
		Another silence. I almost thought I could tell his smoky exhalations were increasing in temperature, but his mood was very hard to read aside from his usual concentrated anger. The only thing I could muse upon at this point was realizing Cygnis had only stared at me meaningfully - never lecherously, and certainly never unnervingly...
		
		\c[TCol_Cygnis]''It is not your fault. I am a world apart from a woman with whom I share difficult relations, but every time you enter my vision I am reminded of her... and my mistakes in life. I apologize.''\c-
		
		The next bout of relative quiet left me surprised. It was not my intention to dig deeper, but I could not resist letting my next words spill from my mouth.
		
		\c[TCol_Visse]''... A lover?''\c-
		
		Cygnis snorted. It was nearly spontaneous enough that I could have interpreted it as a laugh.
		
		\c[TCol_Cygnis]''A succubus. I have a wife.''\c-
		
		That was all the explanation I was getting, it seemed. Cygnis made no further attempt to clarify as his boots tromped heavily away, leaving me to ruminate on this little tidbit of information. I was honestly not sure how to take it further but since this conversation resulted in a vast reduction of Cygnis' stares I considered the matter to be closed. Perhaps it is not so bad to be glanced at every now and then in a more neutral manner than some other occupants of this ship have sometimes...";
	}
}
