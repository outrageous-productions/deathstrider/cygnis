Model ArcanumSigilAdd
{
	Path "Models/Arcanum" Model 0 "ArcanumPlane.obj" DONTCULLBACKFACES USEACTORPITCH
	Scale 1.0 1.0 1.0
	SurfaceSkin 0 0 "DSCygnisBloodshieldSigil_d.png" FrameIndex BLDS H 0 0
	SurfaceSkin 0 0 "DSCygnisDimensionalDoorSigil_d.png" FrameIndex DOOR T 0 0
	SurfaceSkin 0 0 "DSCygnisFrostbiteSigil_d.png" FrameIndex FROS T 0 0
}